FROM container4armhf/armhf-alpine:3.5

COPY qemu-arm-static /usr/bin/

WORKDIR /opt

RUN echo -ne "http://nl.alpinelinux.org/alpine/v3.5/community\nhttp://build.svcs.io/artifacts/alpine\n" >> /etc/apk/repositories && \
  apk add --no-cache --allow-untrusted gcc g++ make git linux-headers curl ca-certificates zlib-dev openssl-dev protobuf-dev protobuf grpc grpc-dev && update-ca-certificates && \
  git clone git://git.drogon.net/wiringPi && \
  cd wiringPi/wiringPi && \
  make install-static && \
  mkdir /opt/app && \
  git clone git://github.com/gioblu/PJON.git

COPY *.cpp *.h *.proto Makefile /opt/app/
COPY PJON /opt/app/PJON

RUN cd /opt/app && make
